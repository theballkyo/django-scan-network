import ipaddress
import os
import queue
from threading import Thread

import netaddr
from django.shortcuts import render


def pingv4(ip: ipaddress.IPv4Address):
    if os.name == 'posix':
        return os.system("ping -c 1 -W 1 {ip} > /dev/null".format(ip=str(ip))) == 0
    return False


def scan_network(network: iter):
    result = {}
    input_queue = queue.Queue()

    class PingWorker(Thread):
        def __init__(self, input_queue, result):
            super().__init__()
            self.input_queue = input_queue
            self.result = result

        def run(self):
            while True:
                ip = self.input_queue.get()
                isalive = pingv4(ip)
                result[str(ip)] = isalive
                self.input_queue.task_done()

    for _ in range(50):
        p = PingWorker(input_queue, result)
        p.setDaemon(True)
        p.start()

    for ip in network:
        result[str(ip)] = False
        input_queue.put(ip)

    input_queue.join()

    return result


def index(request):
    network = request.GET.get('network')
    start_ip = request.GET.get('start_ip')
    end_ip = request.GET.get('end_ip')
    result = {}
    if network:
        result = scan_network(ipaddress.IPv4Network(network))

    elif start_ip and end_ip:
        ip_range = netaddr.IPRange(start_ip, end_ip)
        print(ip_range)
        result = scan_network(ip_range)
        print(result)

    return render(request, "discover/index.html", {
        'result': result,
        'network': network,
        'start_ip': start_ip,
        'end_ip': end_ip
    })
